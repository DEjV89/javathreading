package pl.dejv.javathreading;

/**
 * Created by dejv on 11/11/2017.
 */

public class GeneratorPi {

    private boolean negative = true;
    private double pi; // Initializes to 0.0, by default

    public void calculate() {
        for (int i = 3; i < 100000; i += 2) {
            if (negative)
                pi -= (1.0 / i);
            else
                pi += (1.0 / i);
            negative = !negative;
        }
        pi += 1.0;
        pi *= 4.0;
        System.out.println("Finished calculating PI : " + pi);
    }

    public double getPi() {
        return pi;
    }
}
